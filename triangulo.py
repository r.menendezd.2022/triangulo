import sys

def line(numero:int):
    linea=""
    for j in range (1,numero+1):
        linea+=str(numero)
    linea+="\n"
    return(linea)

def triangle(numero: int):
    cadena=""
    for i in range(1,numero+1):
        cadena+=line(i)
    return(cadena)

def main():
    if len(sys.argv)<2:
        print("El numero tiene que ser mayor")
        sys.exit(1)
    else:
        numero: int = sys.argv[1]
        if int (numero)>0 and int(numero)<10:
            piramide = triangle(int(numero))
            print(piramide)
        else:
            print("ValueError")


if __name__ == '__main__':
    main()

